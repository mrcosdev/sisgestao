CREATE SCHEMA `sisgestao` DEFAULT CHARACTER SET latin1 ;

CREATE TABLE `sisgestao`.`providers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(10) NULL DEFAULT 0,
  `name` VARCHAR(100) NULL,
  `address` VARCHAR(200) NULL,
  `phone` VARCHAR(45) NULL,
  `email` VARCHAR(150) NULL,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));


CREATE TABLE `sisgestao`.`customers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(10) NULL DEFAULT 0,
  `name` VARCHAR(100) NULL,
  `address` VARCHAR(200) NULL,
  `phone` VARCHAR(45) NULL,
  `email` VARCHAR(150) NULL,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));


CREATE TABLE `sisgestao`.`products` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(10) NULL DEFAULT 0,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(300) NULL,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `idx_code` (`code` ASC));


CREATE TABLE `sisgestao`.`vehicles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(10) NULL,
  `name` VARCHAR(45) NULL,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));


CREATE TABLE `sisgestao`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `email` VARCHAR(150) NULL,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `idx_email` (`email` ASC));

CREATE TABLE `sisgestao`.`products_providers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `products_id` INT NULL,
  `providers_id` INT NULL,
  `unit` VARCHAR(5) NULL DEFAULT 'm3',
  `price` DECIMAL(8,4) NULL DEFAULT 0,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `idx_products_id` (`products_id` ASC),
  INDEX `idx_providers_id` (`providers_id` ASC));

CREATE TABLE `sisgestao`.`products_customers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `products_id` INT NULL,
  `customers_id` INT NULL,
  `unit` VARCHAR(5) NULL DEFAULT 'm3',
  `price` DECIMAL(8,4) NULL DEFAULT 0,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `idx_products_id` (`products_id` ASC),
  INDEX `idx_customers_id` (`customers_id` ASC));



CREATE TABLE `sisgestao`.`activities` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `providers_id` INT NULL,
  `provider_code` VARCHAR(10) NULL,
  `provider_name` VARCHAR(100) NULL,
  `products_providers_id` INT NULL,
  `num_vale` VARCHAR(10) NULL,
  `products_id` INT NULL,
  `product_code` VARCHAR(10) NULL,
  `qty_withdrawn` DECIMAL(8,4) NULL DEFAULT 0,
  `price_unit` DECIMAL(8,4) NULL DEFAULT 0,
  `total_c` DECIMAL(8,4) NULL DEFAULT 0,
  `vehicles_id` INT NULL,
  `vehicle_code` VARCHAR(10) NULL,
  `vehicle_name` VARCHAR(45) NULL,
  `customers_id` INT NULL,
  `customer_name` VARCHAR(100) NULL,
  `dt_delivery` TIMESTAMP NULL,
  `week_number` INT NULL,
  `qty_delivery` DECIMAL(8,4) NULL DEFAULT 0,
  `delivery_price_unit` DECIMAL(8,4) NULL DEFAULT 0,
  `total_v` DECIMAL(8,4) NULL DEFAULT 0,
  `gross_margin` DECIMAL(8,4) NULL DEFAULT 0,
  `delivery_fee` DECIMAL(8,4) NULL DEFAULT 0,
  `km_start` DECIMAL(9,2) NULL DEFAULT 0,
  `km_end` DECIMAL(9,2) NULL DEFAULT 0,
  `km_rolled` DECIMAL(9,2) NULL DEFAULT 0,
  `gas_qty` DECIMAL(8,4) NULL DEFAULT 0,
  `gas_price` DECIMAL(8,4) NULL DEFAULT 0,
  `lubricant_value` DECIMAL(8,4) NULL DEFAULT 0,
  `filter_value` DECIMAL(8,4) NULL DEFAULT 0,
  `tires_value` DECIMAL(8,4) NULL DEFAULT 0,
  `parts_value` DECIMAL(8,4) NULL DEFAULT 0,
  `mechanical_value` DECIMAL(8,4) NULL DEFAULT 0,
  `ipva_value` DECIMAL(8,4) NULL DEFAULT 0,
  `insurance_value` DECIMAL(8,4) NULL DEFAULT 0,
  `permit_other_value` DECIMAL(8,4) NULL DEFAULT 0,
  `total_expenses` DECIMAL(8,4) NULL DEFAULT 0,
  `value_per_km_rolled` DECIMAL(8,4) NULL DEFAULT 0,
  `img_hash` VARCHAR(150) NULL,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`));

ALTER TABLE `sisgestao`.`activities` 
ADD COLUMN `products_customers_id` INT NULL AFTER `modified`;

