module.exports = {
    admin_crm: {
        _name: 'admin_crm',
        _extension: 'js',
        _components_names: [
            'CrmAdminCustomers'
        ],
        app: ['js/src/apps/admin_crm_customers.js']
    },
    brand: {
        _name: 'brand',
        _extension: 'js',
        _components_names: [
            'User',
            'BrowserAPIs',
            'StoresList',
            'BrandStoreFinder'
        ],
        app: ['js/src/apps/brand.js']
    },
    checkout: {
        _name: 'checkout',
        _extension: 'js',
        _components_names: [
            'ScheduleOrder',
            'BrowserAPIs',
            'User',
            'CheckoutInfo',
            'BillingAddressModal',
            'AddressModalHelper',
            'AddressModal',
            'AddressMapModal',
            'CheckoutList',
            'Checkout',
            'ListPaymentForms',
            'ListAddresses',
            'MobileNavbar',
            'RegisterContact',
            'RegisterInvoice',
            'RegisterPaymentForm',
            'VoucherForm',
            'LoyaltyprogramInfo',
            'ConfirmOrder',
            'ListBusinessHours',
            'UserCheckout'
        ],
        app: ['js/src/apps/checkout.js']
    },
    facebook_more_details: {
        _name: 'facebook_more_details',
        _extension: 'js',
        _components_names: [
            'User'
        ],
        app: ['js/src/apps/facebook_more_details.js']
    },
    checkout_done: {
        _name: 'checkout_done',
        _extension: 'js',
        _components_names: [
            'ScheduleOrder',
            'BrowserAPIs',
            'User',
            'StoreUsers',
            'MobileNavbar',
            'CheckoutInfo',
            'AddressModalHelper',
            'AddressModal',
            'AddressMapModal',
            'StoreDeliveryArea',
            'StoreInfo',
            'CheckoutList',
            'Checkout',
            'CheckoutInfo',
            'ListBusinessHours',
            'Rapiddo/PackageTracker', // Omitting extension
            'VoucherForm'
        ],
        app: ['js/src/apps/checkout_done.js']
    },
    admin_expenses: {
        _name: 'admin_expenses',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'SwitchEnableDelivery',
            'StatusNotification',
            // 'ExpensesMonthPanel',
            // 'ExpensesMonthsList',
            // 'ExpensesYearsList',
            'Expenses'

        ],
        app: ['js/src/apps/admin_expenses.js']
    },
    admin_store_stats: {
        _name: 'admin_store_stats',
        _extension: 'js',
        _components_names: [
            'Payment',
            'BrowserAPIs',
            'SwitchEnableDelivery',
            'StatusNotification',
            'ManageAds',
            'ManageAds/Stats',
            'Payment/Checkout',
            'StoreStats'
        ],
        app: ['js/src/apps/admin_store_stats.js']
    },
    admin_promotion: {
        _name: 'admin_promotion',
        _extension: 'js',
        _components_names: [
            'Payment/Checkout',
            'ManageAds/Balance',
            'ManageAds/Facebook',
            'ManageAds/Google',
            'BrowserAPIs',
            'SwitchEnableDelivery',
            'StatusNotification',
            'PushNotificationModal',
            'FacebookPostModal',
            'PromotionMailcampaignsModal',
            'PromotionModal',
            'ManageLoyalties',
            'ManageVouchers',
            'ListStoreConfigs',
            'ManageVouchers',
            'Promotion'
        ],
        app: ['js/src/apps/admin_promotion.js']
    },
    admin_pushes: {
        _name: 'admin_pushes',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'SwitchEnableDelivery',
            'StatusNotification',
            'PushNotificationModal',
            'PushesPanel'
        ],
        app: ['js/src/apps/admin_pushes.js']
    },
    admin_loyaltyprograms_users: {
        _name: "admin_loyaltyprograms_users",
        _extension: "js",
        _components_names: [
            "LoyaltyprogramsUsers"
        ],
        app: ["js/src/apps/admin_loyaltyprograms_users.js"]
    },
    admin_bills: {
        _name: "admin_bills",
        _extension: "js",
        _components_names: [
            "Bills"
        ],
        app: ["js/src/apps/admin_bills.js"]
    },
    admin_vouchers_use: {
        _name: "admin_vouchers_use",
        _extension: "js",
        _components_names: [
            "VouchersUser"
        ],
        app: ["js/src/apps/admin_vouchers_use.js"]
    },
    admin_stripe_payments: {
        _name: 'admin_stripe_payments',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'SwitchEnableDelivery',
            'StatusNotification',
            'StripeMenu',
            'StripePayments'
        ],
        app: ['js/src/apps/admin_stripe_payments.js']
    },
    store_user: {
        _name: "store_user",
        _extension: "js",
        _components_names: [
            'ScheduleOrder',
            'StoreUserStatus',
            "CardsList",
            'User',
            'AddressModalHelper',
            'AddressModal',
            'AddressMapModal',
            'CheckoutInfo',
            'CheckoutList',
            'StoreUsers',
            'MobileNavbar',
            'Checkout',
            'UserLastOrders',
            'UserManageAddresses',
            'ListRelatedStores',
            'UserOptouts'
        ],
        app: ["js/src/apps/store_user.js"]
    },
    admin_menu: {
        _name: 'admin_menu',
        _extension: 'js',
        _components_names: [
            "AdminPermissionHelper",
            'ManageMenuItem',
            'ManageItemProperty',
            'ManageItemAvailability',
            'ItemAvailability',
            'ManageGlobalProperty',
            'ManageGlobalOption',
            'ManageMenu',
            'BrowserAPIs',
            'TablePanel',
            'TableItems',
            'ManageItem',
            'MenuSearch',
            'ListCategoriesDraggable',
            'AdminHelp',
            'AdminToDos'
        ],
        app: ['js/src/apps/admin_menu.js']
    },
    admin_global: {
        _name: 'admin_global',
        _extension: 'js',
        _components_names: [
            "AdminPermissionHelper",
            'BrowserAPIs',
            'StatusNotification',
            'SwitchEnableDelivery',
            'ManageMenuItem',
            'ManageItemProperty',
            'ManageItemAvailability',
            'ManageMenu',
            'ManageGlobalProperty',
            'ManageGlobalOption',
            'MenuSearch',
            'TableItems',
            'PropertiesOptions'
        ],
        app: ['js/src/apps/admin_global.js']
    },
    admin_store: {
        _name: 'admin_store',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'StoreGeneral',
            'Colorpicker',
            'ListStoreConfigs',
            'TableStoreSchedule',
            'DeliveryDetails/ManageLoggi',
            'DeliveryDetails/ManageRapiddo',
            'DeliveryDetails/DeliveryDetails',
            'CustomizeStore',
            'ManageNpsSurveys/NpsSurveys', // Omitting extension
            'ManageNpsSurveys/NpsSurveyQuestions',  // Omitting extension
            'ManageNpsSurveys/ManageNpsSurveys',  // Omitting extension
            'ManagePayments',
            'ManageInvoiceClientDocument',
            'ManageEmployees',
            'ManageDeliverers',
            'ManageApps',
            'PromotionModal',
            'ManageLoyalties',
            'ManageVouchers',
            'ManagePrinters',
            'ManageOnlinePayment/FieldsNeeded',
            'PaymentOptions/OnlineOptions', /* Required in ManageOnlinePayment */
            'ManageOnlinePayment',
            'AdminHelp',
            'AdminToDos',
            'PushNotificationModal',
            'PushesPanel'
        ],
        app: ['js/src/apps/admin_store.js']
    },
    admin_survey: {
        _name: 'admin_survey',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'StatusNotification',
            'NpsServiceStats'
        ],
        app: ['js/src/apps/admin_survey.js']
    },
    admin_modules: {
        _name: 'admin_modules',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'Modules',
            'ModulesList'
        ],
        app: ['js/src/apps/admin_modules.js']
    },
    admin_stores_list: {
        _name: 'admin_stores_list',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'StoresList'
        ],
        app: ['js/src/apps/admin_stores_list.js']
    },
    experience_single: {
        _name: 'experience_single',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'Landing',
            'Header',
            'FormExperienceContact'
        ],
        app: ['js/src/apps/experience_single.js']
    },
    index: {
        _name: 'index',
        _extension: 'js',
        _components_names: [
            'ScheduleOrder',
            'User',
            'LoyaltyprogramInfo',
            'BrowserAPIs',
            'ItemProperties',
            'ItemsList',
            'CategoriesBlock',
            'CheckoutInfo',
            'StoreUserStatus',
            'AddressModalHelper',
            'AddressModal',
            'AddressMapModal',
            'CheckoutList',
            'MobileNavbar',
            'ItemsBlock',
            'Checkout',
            'User',
            'VoucherForm',
            'Categories',
            'OrderBlock',
            'ProceedToCheckout',
            'ListBusinessHours',
            'StoreDeliveryArea',
            'ListAddresses',
            'StoreInfo'
        ],
        app: ['js/src/app.js']
    },
    landing: {
        _name: 'landing',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'Landing',
            'Header',
            'Ebook'
        ],
        app: ['js/src/apps/landing.js']
    },
    landingflow: {
        _name: 'landingflow',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'Colorpicker',
            'Landingflow'
        ],
        app: ['js/src/apps/landingflow.js']
    },
    partners: {
        _name: 'partners',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'Header',
            'Landing',
            'Partners'
        ],
        app: ['js/src/apps/partners.js']
    },
    manager: {
        _name: 'manager',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'Colorpicker',
            'Manager',
            'LeadsStats',
            'CustomersPanel',
            'CustomerStats',
            'ResellersPayments',
            'DebugOrders',
            'DebugZipcodes',
            'Storemessages',
            'UpdateStorePinLocation',
            'Contractagreementinfos',
            'Contractagreements',
            'ManagerAds',
            'Subscription',
            'Superlogica',
            'Terms',
            'SlackMessages',
            'WebSocketsStores'
        ],
        app: ['js/src/apps/manager.js']
    },
    net_promoter_score: {
        _name: 'net_promoter_score',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'NPSSurvey'
        ],
        app: ['js/src/apps/net_promoter_score.js']
    },
    nps_stats: {
        _name: 'nps_stats',
        _extension: 'js',
        _components_names: [],
        app: ['js/src/apps/nps_stats.js']
    },
    mailcampaign_optout: {
        _name: 'mailcampaign_optout',
        _extension: 'js',
        _components_names: [
            'MailcampaignOptout'
        ],
        app: ['js/src/apps/mailcampaign_optout.js']
    },
    reset_password: {
        _name: 'reset_password',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'ResetPassword'
        ],
        app: ['js/src/apps/reset_password.js']
    },
    admin_header: {
        _name: 'admin_header',
        _extension: 'js',
        _components_names: [
            'BrowserAPIs',
            'ManageEmployees',
            'StatusNotification',
            'SwitchEnableDelivery',
            'StripePayments',
            'Storemessages',
            'Superlogica'
        ],
        app: ['js/src/apps/admin_header.js']
    }
};
