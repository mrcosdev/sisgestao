// gulpfile.js
// @author: Matheus Cesario <matheus.cesario@gmail.com>

//
// Plugins
//

'use strict';

// Loading useful plugins
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    concatcss = require('gulp-concat-css'),
    urladjuster = require('gulp-css-url-adjuster'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    map = require('map-stream'),
    del = require('del'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    babelify = require('babelify'),
    inlineCss = require('gulp-inline-css'),
    fs = require('fs');

gulp.task('default', ['all', 'watch-all', 'watch-react']);

//
// Sass
//

// Compiling SASS files
gulp.task('styles', function() {
  return sass('sass/base.sass', { style: 'expanded' })
    .pipe(gulp.dest('css/dist/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('css/dist/'))
    .pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('styles-landing', function() {
  return sass('sass/site.sass', { style: 'expanded' })
    .pipe(gulp.dest('css/dist/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('css/dist/'))
    .pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('styles-landingflow', function() {
  return sass('sass/siteflow.sass', { style: 'expanded' })
    .pipe(gulp.dest('css/dist/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('css/dist/'))
    .pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('styles-manager', function() {
  return sass('sass/manager.sass', { style: 'expanded' })
    .pipe(gulp.dest('css/dist/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('css/dist/'))
    .pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('styles-nps-survey', function() {
  return sass('sass/nps-survey.sass', { style: 'expanded' })
    .pipe(gulp.dest('css/dist/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('css/dist/'))
    .pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('styles-store-user', function() {
  return sass('sass/store-user.sass', { style: 'expanded' })
    .pipe(gulp.dest('css/dist/'))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('css/dist/'))
    .pipe(notify({ message: 'Styles task complete' }));
});

//
// Css
//

gulp.task('compile-external-css', function() {

    // Bower components
    var srcs = [
        'css/src/material-modified.css',
        'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/leaflet/dist/leaflet.css',
        'bower_components/flatpickr/dist/flatpickr.min.css',
        'bower_components/leaflet-draw/dist/leaflet.draw.css',
        'bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css',
        'bower_components/cropper/dist/cropper.min.css',
        'bower_components/bootstrap-select/dist/css/bootstrap-select.css',
        'bower_components/datatables/media/css/dataTables.bootstrap.css',
        'bower_components/fancyBox/source/jquery.fancybox.css',
        'bower_components/typeahead.js-bootstrap3.less/typeaheadjs.css',
        'bower_components/sweetalert/dist/sweetalert.css',
        'bower_components/smartbanner.js/dist/smartbanner.min.css'
    ];

    return compileExternalCss({
        'src': srcs
    });
});

// Moving CSS maps to css/dist/utils
gulp.task('move-external-css-maps', function() {

    var srcs = [
        'bower_components/bootstrap-select/dist/css/bootstrap-select.css.map'
    ];

    var name = "/bootstrap-select.css.map";
    var dest = "css/dist/utils/";

    return gulp.src(srcs, {base: '.'})
        .pipe(rename(name))
        .pipe(gulp.dest(dest));
});

function compileExternalCss(data){

    data.name = data.name || "utils.css";
    data.dest = "css/dist/utils/";
    //urladjuster
    return gulp.src(data.src, {base: "."})
        .pipe(concatcss(data.name))
        .pipe(urladjuster({prepend: '/'})) //Deixa as url de imagens relativas à pasta do  bower
        .pipe(gulp.dest(data.dest))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest(data.dest))
        .pipe(notify({ message: 'External Css task complete' }))
}

//
// Javascript
//

/**
 * Putting bundle to compiling pipeline.
 * @param  {Object} data Object containing target files list and destiny file name
 * @return {void}
 */
function compileScripts(data) {
    data.name = data.name || 'scripts.js';  // Optional param
    return gulp.src(data.src)
        // .pipe(jshint())
        // .pipe(jshint.reporter('jshint-stylish'))
        .pipe(concat(data.name))  // Concating all files into `scripts.js`
        .pipe(gulp.dest(data.dest))  // Saving concatenated file into destiny
        .pipe(rename({suffix: '.min'}))  // Adding `.min` into file name
        .pipe(uglify())  // Uglifying script
        .pipe(gulp.dest(data.dest))  // Saving uglified file into destiny
        .pipe(notify({ 'message': [data.name, 'succesfully compiled!'].join(' ') }));
}

/**
 * Compiling all components from `src/components`
 * @return {[type]} [description]
 */
gulp.task('compile-js-components', function() {

    // main path
    var path = 'js/src/components';

    // Components (sorted due dependencies issues)
    var srcs = [
        'ItemProperties',
        'itemsList',
        'CategoriesBlock',
        'checkoutInfo',
        'ManageDeliverers',
        'BillingAddressModal',
        'AddressModal',
        'AddressMapModal',
        'checkoutList',
        'VoucherForm',
        'listCategories',
        'MobileNavbar',
        'itemsBlock',
        'ordersList',
        'orderDetail',
        'ordersBlock',
        'OrderBlock',
        'ProceedToCheckout',
        'ListBusinessHours',
        'Expenses'
    ];

    return compileScripts({
        'src': srcs.map(function(src) { return [path, src, '*.js'].join('/'); }),
        'dest': 'js/dist/components'
    });
});

// Admin Components
gulp.task('compile-js-admin-components', function() {

    // main path
    var path = 'js/src/admin/components';

    // Components (sorted due dependencies issues)
    var srcs = [

        /* Single components*/
        '../../components/Rapiddo/PackageTracker.js',

        /* Whole folder */
        '../../components/SwitchEnableDelivery/*.js',
        '../../components/StatusNotification/*.js',
        '../../components/Loggi/*.js',
        '../../components/ManageDeliverers/*.js',
        'OrderEvents/*.js',
        'ordersList/*.js',
        'orderDetail/*.js',
        'RealtimeFirebase/*.js',
        'ordersBlock/*.js',
        'AdminOrderInput/*.js',
        'AdminNpsRequest/*.js'
    ];

    return compileScripts({
        'src': srcs.map(function(src) { return [path, src].join('/'); }),
        'dest': 'js/dist/admin/components'
    });
});

// All Shared
gulp.task('compile-js-shared', function() {
    return compileScripts({
        "src": "js/src/shared/**/*.js",
        "dest": "js/dist/shared"
    });
});

// Main app
gulp.task('compile-js-app', function() {
    return compileScripts({
        "src": "js/src/app.js",
        "dest": "js/dist/app"
    });
});

// Main admin app
gulp.task('compile-js-admin-app', function() {
    return compileScripts({
        "src": "js/src/admin/app.js",
        "dest": "js/dist/admin/app"
    });
});

// Compiling Javascript utils

gulp.task('compile-js-utils', function() {

    // Bower components
    // Concating all these files into `js/dist/utils/scripts.js`
    var srcs = [
        'bower_components/jwt-decode/build/jwt-decode.min.js',
        'bower_components/jquery/dist/jquery.js',
        'bower_components/jquery-ui/jquery-ui.min.js',
        'bower_components/jquery-ui/ui/i18n/datepicker-pt-BR.js',
        'bower_components/moment/moment.js',
        'bower_components/moment/locale/pt-br.js',
        'bower_components/mustache.js/mustache.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/leaflet/dist/leaflet.js',
        'bower_components/leaflet-draw/dist/leaflet.draw.js',
        'bower_components/leaflet-heat/index.js',
        'bower_components/flatpickr/dist/flatpickr.js',
        'bower_components/js-cookie/src/js.cookie.js',
        'bower_components/cropper/dist/cropper.min.js',
        'bower_components/polyglot/build/polyglot.js',
        'bower_components/Chart.js/dist/Chart.js',
        'bower_components/bootstrap-select/dist/js/bootstrap-select.js',
        'bower_components/datatables/media/js/jquery.dataTables.js',
        'bower_components/datatables/media/js/dataTables.bootstrap.js',
        'bower_components/fancyBox/source/jquery.fancybox.pack.js',
        'bower_components/numeral/numeral.js',
        'bower_components/numeral/languages/pt-br.js',
        'bower_components/typeahead.js/dist/bloodhound.min.js',
        'bower_components/typeahead.js/dist/typeahead.bundle.min.js',
        'bower_components/typeahead.js/dist/typeahead.jquery.min.js',
        'bower_components/sweetalert/dist/sweetalert.min.js',
        'bower_components/mailcheck/src/mailcheck.min.js',
        'bower_components/smartbanner.js/dist/smartbanner.min.js',
        'bower_components/material-design-lite/material.min.js',
        'js/utils/**/*.js'
    ];

    return compileScripts({
        "src": srcs,
        "dest": "js/dist/utils"
    });
});

gulp.task('compile-js-qz-1.9', function() {

    // QZ 1.9
    // Concating all these files into `js/dist/qz-1.9/scripts.js`
    var srcs = [
        'js/qz-1.9/*.js'
    ];

    return compileScripts({
        "src": srcs,
        "dest": "js/dist/qz-1.9"
    });
});

gulp.task('compile-js-qz-2.0', function() {

    // QZ 2.0
    // Concating all these files into `js/dist/qz-2.0/scripts.js`
    var srcs = [
        'js/qz-2.0/*.js'
    ];

    return compileScripts({
        "src": srcs,
        "dest": "js/dist/qz-2.0"
    });
});

//
// Watchers
//

function initializeLiveReload() {

    // Create LiveReload server
    livereload.listen();

    // Watch any files in dist/, reload on change
    gulp.watch(['css/dist/**']).on('change', livereload.changed);

    // Watch any files in dist/, reload on change
    gulp.watch(['js/dist/**']).on('change', livereload.changed);
}

gulp.task('watch-all', function() {

    //
    // Compiling scripts
    //

    // Watch .sass files
    gulp.watch('sass/**/*.sass', [
        'styles',
        'styles-landing',
        'styles-landingflow',
        'styles-manager',
        'styles-nps-survey',
        'styles-store-user'
    ]);

    // Watch .js files (components)
    gulp.watch('js/src/components/**/*.js', [
        'compile-js-components',
        'js-compile-admin_menu',
        'js-compile-admin_global',
        'js-compile-admin_crm',
        'js-compile-brand',
        'js-compile-checkout',
        'js-compile-facebook_more_details',
        'js-compile-checkout_done',
        'js-compile-admin_expenses',
        'js-compile-admin_store_stats',
        'js-compile-admin_promotion',
        'js-compile-admin_pushes',
        'js-compile-admin_loyaltyprograms_users',
        'js-compile-admin_bills',
        'js-compile-admin_vouchers_use',
        'js-compile-admin_modules',

        'js-compile-admin_stripe_payments',
        'js-compile-store_user',

        'js-compile-admin_store',
        'js-compile-admin_survey',
        'js-compile-admin_modules',
        'js-compile-admin_stores_list',
        'js-compile-experience_single',
        'js-compile-index',
        'js-compile-landing',
        'js-compile-landingflow',
        'js-compile-partners',
        'js-compile-manager',
        'js-compile-net_promoter_score',
        'js-compile-nps_stats',
        'js-compile-mailcampaign_optout',
        'js-compile-reset_password',
        'js-compile-admin_header',
        'js-watch-admin_crm'
    ]);
    gulp.watch('js/src/admin/components/**/*.js', ['compile-js-admin-components']);
    gulp.watch('js/src/shared/**/*.js', ['compile-js-shared']);
    gulp.watch('js/src/app.js', ['compile-js-app']);
    gulp.watch('js/src/admin/app.js', ['compile-js-admin-app']);

    gulp.watch('css/src/email.css', ['inline-css'])

    initializeLiveReload();  // LiveReload
});

gulp.task("watch-sass", function() {
    gulp.watch('sass/**/*.sass', [
        'styles',
        'styles-landing',
        'styles-landingflow',
        'styles-manager',
        'styles-nps-survey',
        'styles-store-user'
    ]);
    initializeLiveReload();
});

//
// Bundles (apps)
//

/**
 * Appending full project path for each component source.
 * If `src` has slash ("/"), then we consider it a single component and
 * do not append the suffix "*.js" (all files)
 * @param  {String} src Path to files
 * @return {String}     Full path to files
 */
function _mapComponents(src) {

    // Src is path to file
    if (src.indexOf('/') > -1) {
      return ['js/src/components/', src, '.js'].join('');
    }

    // Src is path to folder
    return ['js/src/components', src, '**/*.js'].join('/');
}

/**
 * Compiling all scripts from bundle (PageComponentMap element).
 * ATTENTION: this function is binded with the bundle (PageComponentMap element).
 * @return void
 */
function compileBundleScripts() {

    compileScripts({
        'dest': 'js/dist/app',
        'name': [this._name, this._extension].join('.'),
        'src': this.app
    });

    compileScripts({
        'dest': 'js/dist/components',
        'name': [this._name, this._extension].join('.'),
        'src':  this._components_names.map(_mapComponents)
    });

    return;
}

/**
 * Creating watcher file for bundle components.
 * ATTENTION: this function is binded with the bundle (PageComponentMap element).
 * @return void
 */
function watcherBundleComponents() {
    gulp.watch(
        this._components_names.map(_mapComponents).concat(this.app),
        ['js-compile-' + this._name]
    );
}

// Creating compiler and watcher for bundles

var bundles = require('./PageComponentMap.js');
var allTasks = [];

// For each bundle, creates both compiler and watcher tasks
for (var elem in bundles) {
    if(bundles[elem]) {
        allTasks.push('js-compile-' + bundles[elem]._name);

        // Creating compiler task
        gulp.task(
            'js-compile-' + bundles[elem]._name,
            compileBundleScripts.bind(bundles[elem])
        );

        // Creating watcher task
        gulp.task(
            'js-watch-' + bundles[elem]._name,
            watcherBundleComponents.bind(bundles[elem])
        );
    }
}

//
// Watchers
//

gulp.task('js-watch-admin_orders',  function() {
    gulp.watch([
        'js/src/admin/components/**/*.js',
        'js/src/components/**/*.js'
    ], [
        'compile-js-admin-components'
    ]);
});


//
// ReactJS
//

gulp.task('build-react', function () {
    let pages = [
        'third_services_status'
    ];

    for (let i = 0; i < pages.length; i++) {
        compileReactJS(pages[i]);
    }
});

gulp.task('watch-react', ['build-react'], function () {
    gulp.watch('js/src/react/**/*.js', ['build-react']);
});

function compileReactJS(page) {
    return browserify({entries: 'js/src/react/pages/' + page + '.js', debug: true})
        .transform('babelify', {presets: ['es2015', 'react']})
        .bundle()
        .pipe(source(page + '.js'))
        .pipe(gulp.dest('js/dist/react'));
}


//
// CSS inliner
//

gulp.task('inline-css', function() {
    return gulp.src('../src/Template/Email/rawHtml/*.mustache') // source
        .pipe(inlineCss({
            extraCss: fs.readFileSync('./css/src/email.css', 'utf8'), // css
        }))
        .pipe(gulp.dest('../src/Template/Email/html/')); // destination
});

//
// All tasks
//

allTasks = allTasks.concat([
    'styles',
    'styles-landing',
    'styles-landingflow',
    'styles-manager',
    'styles-nps-survey',
    'styles-store-user',
    'js-compile-admin_crm',
    'js-compile-brand',
    'js-compile-checkout',
    'js-compile-facebook_more_details',
    'js-compile-checkout_done',
    'js-compile-admin_expenses',
    'js-compile-admin_store_stats',
    'js-compile-admin_pushes',
    'js-compile-admin_loyaltyprograms_users',
    'js-compile-admin_bills',
    'js-compile-admin_vouchers_use',
    'js-compile-admin_stripe_payments',
    'js-compile-store_user',
    'js-compile-admin_menu',
    'js-compile-admin_global',
    'js-compile-admin_store',
    'js-compile-admin_survey',
    'js-compile-admin_modules',
    'js-compile-admin_stores_list',
    "js-compile-admin_promotion",
    "js-compile-mailcampaign_optout",
    'js-compile-experience_single',
    'js-compile-index',
    'js-compile-landing',
    'js-compile-landingflow',
    'js-compile-partners',
    'js-compile-manager',
    'js-compile-net_promoter_score',
    'js-compile-nps_stats',
    'js-compile-reset_password',
    'js-compile-admin_header',
    'compile-js-utils',
    'compile-js-shared',
    'compile-js-components',
    'compile-js-admin-components',
    'compile-js-app',
    'compile-js-admin-app',
    'compile-js-utils',
    'compile-js-qz-1.9',
    'compile-js-qz-2.0',
    'compile-external-css',
    'move-external-css-maps',
    'build-react',
    'inline-css'
]);

gulp.task('all', allTasks);
